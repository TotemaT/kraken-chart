import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Config } from 'src/config';

@Module({
    imports: [MongooseModule.forRoot(Config.mongo.host)],
})
export class DatabaseModule {}
