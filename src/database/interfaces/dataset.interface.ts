import { Document } from 'mongoose';

export interface Dataset extends Document {
    label: string;
    data: number[];
}
