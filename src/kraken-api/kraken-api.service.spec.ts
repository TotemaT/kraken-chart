import { Test, TestingModule } from '@nestjs/testing';
import { KrakenApiService } from './kraken-api.service';

describe('KrakenApiService', () => {
  let service: KrakenApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [KrakenApiService],
    }).compile();

    service = module.get<KrakenApiService>(KrakenApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
