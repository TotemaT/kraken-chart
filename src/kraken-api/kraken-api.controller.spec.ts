import { Test, TestingModule } from '@nestjs/testing';
import { KrakenApiController } from './kraken-api.controller';

describe('KrakenApi Controller', () => {
  let controller: KrakenApiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KrakenApiController],
    }).compile();

    controller = module.get<KrakenApiController>(KrakenApiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
