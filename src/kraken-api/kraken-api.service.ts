import { Injectable, HttpService } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';

import { Model } from 'mongoose';
import * as crypto from 'crypto';
import * as querystring from 'querystring';
import { Config } from '../config';
import { DB } from '../database/schemas';
import { Dataset, Label } from '../database/interfaces';
import * as cron from 'node-cron';

export type Labels = string[];
export type Datasets = { [asset: string]: number[] };
type Headers = { 'API-Key': string; 'API-Sign': string };
type Balance = { [assets: string]: number };
type AssetToValue = { [asset: string]: number };
type AssetToName = { [asset: string]: string };

@Injectable()
export class KrakenApiService {
    private task: cron.ScheduledTask;

    constructor(
        private readonly http: HttpService,
        @InjectModel(DB.datasets)
        private readonly datasetModel: Model<Dataset>,
        @InjectModel(DB.label)
        private readonly labelModel: Model<Label>,
    ) {
        this.task = cron.schedule('0 * * * *', () => {
            this.updateChart();
        });
    }

    private get endpoint(): string {
        return Config.kraken.endpoint;
    }

    private get version(): number {
        return Config.kraken.version;
    }

    private get apiKey(): string {
        return Config.kraken.apiKey;
    }

    private get secret(): string {
        return Config.kraken.secret;
    }

    startCron() {
        this.task.start();
    }

    stopCron() {
        this.task.stop();
    }

    async labels(): Promise<string[]> {
        const label: Label = await this.labelModel.findOne();
        return label ? label.times : [];
    }

    async datasets(): Promise<Dataset[]> {
        return this.datasetModel.find();
    }

    private async updateChart() {
        try {
            const balance = await this.balance();
            const assetsToNames = await this.assetNames(Object.keys(balance));
            const pairs = await this.assetValues(Object.values(assetsToNames));

            Object.entries(assetsToNames).forEach(async ([asset, name]) => {
                const quantity = balance[asset];
                const value = pairs[asset];
                await this.updateAsset(name, quantity * value);
            });

            await this.updateLabels(
                new Date().toLocaleTimeString('fr-BE', {
                    hour: '2-digit',
                    minute: '2-digit',
                }),
            );
        } catch (e) {
            console.error(e);
        }
    }

    private async updateAsset(asset: string, value: number) {
        const dataset: Dataset = await this.datasetModel.findOne({
            label: asset,
        });
        if (dataset) {
            dataset.data.push(value);
            await this.datasetModel.updateOne({ _id: dataset._id }, dataset);
        } else {
            const label: Label = await this.labelModel.findOne();
            const existingCount = label ? label.times.length : 0;
            const values = new Array(existingCount).fill(0);
            await this.datasetModel.create({
                label: asset,
                data: [...values, value],
            });
        }
    }

    private async updateLabels(time: string) {
        const label: Label = await this.labelModel.findOne();
        if (label) {
            label.times.push(time);
            await this.labelModel.updateOne({ _id: label._id }, label);
        } else {
            await this.labelModel.create({
                times: [time],
            });
        }
    }

    private async balance(): Promise<Balance> {
        const uri = this.getUri('private/Balance');
        const nonce = this.nonce;
        const body = {
            nonce,
        };

        const response = await this.http
            .post(`${this.endpoint}${uri}`, querystring.stringify(body), {
                headers: this.headers(uri, body, nonce),
            })
            .toPromise();

        if (response.data.error.length) {
            return Promise.reject(response.data.error);
        } else {
            const balance = response.data.result;
            if (balance['ZEUR']) {
                delete balance['ZEUR'];
            }
            return balance;
        }
    }

    private async assetValues(assets: string[]): Promise<AssetToValue> {
        const uri = this.getUri('public/Ticker');
        const pair = assets.map(asset => `${asset}EUR`).join(',');
        const body = {
            pair,
        };

        const response = await this.http
            .post(`${this.endpoint}${uri}`, querystring.stringify(body))
            .toPromise();

        if (response.data.error.length) {
            return Promise.reject(response.data.error);
        } else {
            const values = {};
            Object.entries(response.data.result).forEach(
                ([pair, data]: [string, any]) => {
                    const name = pair.includes('ZEUR')
                        ? pair.slice(0, -4)
                        : pair.slice(0, -3);
                    values[name] = data.c[0];
                },
            );
            return values;
        }
    }

    private async assetNames(assets: string[]): Promise<AssetToName> {
        const uri = this.getUri('public/Assets');
        const asset = assets.join(',');

        const body = {
            asset,
        };

        const response = await this.http
            .post(`${this.endpoint}${uri}`, querystring.stringify(body))
            .toPromise();

        if (response.data.error.length) {
            return Promise.reject(response.data.error);
        } else {
            const names = {};
            assets.forEach(asset => {
                names[asset] = response.data.result[asset].altname;
            });
            return names;
        }
    }

    private getUri(method: string): string {
        return `/${this.version}/${method}`;
    }

    private get nonce(): number {
        return new Date().getTime();
    }

    private isPrivate(uri: string): boolean {
        return uri.includes('private');
    }

    private headers(uri: string, body: any, nonce: number): Headers {
        return {
            'API-Key': this.apiKey,
            'API-Sign': this.sign(uri, body, nonce),
        };
    }

    private sign(url: string, body: any, nonce: number): string {
        const message = querystring.stringify(body);

        const base64Secret = Buffer.from(this.secret, 'base64');

        const hash = crypto
            .createHash('sha256')
            .update(`${nonce}${message}`)
            .digest('latin1');

        return crypto
            .createHmac('sha512', base64Secret)
            .update(`${url}${hash}`, 'latin1')
            .digest('base64');
    }
}
